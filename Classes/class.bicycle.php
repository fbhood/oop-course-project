<?php

/**
 * Bicycle class extends the Product class
 * @author Fabio P
 * @copyright 2019 Fabio P
 */
class Bicycle extends Product
{
    /**
     * Bicycle Class Variables
     * @var float frameSize Bicycle frame size
     * @var int wheelSize Bicycle wheel size
     *
     */
    public $frameSize;
    public $wheelSize;

    public function __construct($name, $description, $price, $color, $frameSize, $wheelSize)
    {
        parent::__construct($name, $description, $price, $color);
        $this->frameSize = $frameSize;
        $this->wheelSize = $wheelSize;
    }
}
