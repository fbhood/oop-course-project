<?php

/** 
 * Product Class
 * @author Fabio Pacifici
 * @copyright 2019 Fabio Pacific
 * 
 */
class Product
{

    /**
     * Class Variables
     * @var string name The product name
     * @var string description The product description
     * @var float price The product price
     */
    protected $name; // <- this is a public property
    protected $description;
    protected $price;
    public $color;
    public $image;
    public $uri;
    /**
     * Class Constructor
     * @param string $name The product name
     * @param string $description The product description
     * @param float $price The product price
     */
    public function __construct($name, $description, $price, $color = "white", $image = null, $uri = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->color = $color;
        $this->image = $image;
        $this->uri = $uri;
    }
    /**
     * Set a Name
     * @param string $name The product name
     * @return string return $name New product name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $name;
    }


    /**
     * Set a Description
     * @param string $description The description description
     * @return string return $description New product description
     */
    public function setDesciption($description)
    {
        $this->description = $description;
        return $description;
    }

    /**
     * Set the product price
     * @param float $price product price
     * @return float return $price product price
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $price;
    }

    /**
     * Assign a color to the product
     * @param sting $color Product color
     * @return sting return $color product color
     *  */
    public function setColor($color)
    {
        return $this->color = $color;
    }

    /**
     * Assign a image to the product
     * @param sting $image Product image
     * @return sting return $image product image
     *  */
    public function setImage($image)
    {
        return $this->image = $image;
    }



    /** 
     * Set Product URI
     * @param string $uri the produt uri
     * 
     */
    public function setUri($uri)
    {
        return $this->uri = $uri;
    }


    /**
     * Get the product name
     * @return string return name product name
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return float return desc product description
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @return float return price product price
     */
    public function getPrice()
    {
        return $this->price;
    }



    /**
     * Create Product URI
     * @return string returns product uri created
     */
    public function createUri()
    {
        $type = get_class();
        $product_uri = strtolower("single-" . $type . "-" . str_replace(" ", "-", $this->getName()));
        return $product_uri;
    }






    /**
     * Calculate Stock value
     * @param float $price the price
     * @param int $qty Quantity
     * @return float return $calc The total of the calculation
     */
    public static function calcStockValue($price, $qty)
    {
        $calc = $price * $qty;
        return $calc;
    }
    /**
     * Calculate Profit value
     * @param float $price the price
     * @param int $qty Quantity
     * @param float $cost The cost of the item
     * @return float return $totalValue the final value
     */
    public static function calcProfit($price, $qty, $cost)
    {
        $totalCost = $cost * $qty;
        $totalValue = Product::calcStockValue($price, $qty);
        return $totalValue - $totalCost;
    }
}
