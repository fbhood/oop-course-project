<?php
// Check if there are products
if ($products) {

    foreach ($products as $key => $product) {
        // check if the current request uri is euqual to the product uri
        $request = trim($_SERVER["REQUEST_URI"], "/");

        if ($product->uri == $request) {
            // grab product properties
            $name = $product->getName();
            $desciption = $product->getDescription();
            $image = $product->image;
            $price = $product->getPrice();
            // get prev and next links
            $prev = $products[$key - 1]->uri;
            $next = $products[$key + 1]->uri;
            //include the view
            include_once("./Views/single.view.php");
        }
    }
}
