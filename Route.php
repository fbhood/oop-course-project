<?php

/**
 * Route Class
 * @author Fabio Pacifici
 * @copyright 2019 Fabio Pacifici
 * 
 */

class Route
{

    /**
     * Route's class variables
     * @var array routes the application routes
     * */
    protected $routes = [];

    /**
     * Registers the application's routes
     * @param array $routes the application routes
     * @return array return application routes 
     * 
     */
    public function register($routes)
    {
        return $this->routes = $routes;
    }
}
