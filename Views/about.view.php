<?php include("partials/head.php"); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <iframe class="img-fluid h-100" width="560" height="560" src="https://www.youtube.com/embed/aDrohOU6spU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 px-4">
            <h1 class="text-left">PHP CRASH COURSE</h1>
            <p class="lead">
                Welcome to my crash course about Object Oriented Programming in PHP.I will cover the fundamentals of OOP, I will try to use real word examples, no foo bar or value1, value 2 etc. as I realised that they don’t often make much sense to beginners. I will start by covering how to use DocBlocks to document our code, declare a class using the class keyword, declare and access properties including those private and protected, how to use the __constructor() method, how to instantiate an object using the new keyword, class methods, sub-classes, and some extras including magic methods.

            </p>
            <p><a class="btn btn-dark btn-lg" href="https://fabiopacifici.com/object-oriented-php-crash-course/" target="_blank" rel="noopener noreferrer" role="button">Visit the Website</a></p>
        </div>
    </div>
</div>




<?php include("partials/footer.php"); ?>