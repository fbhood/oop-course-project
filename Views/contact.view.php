<?php include("partials/head.php"); ?>


<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <img width="600" height="600" class="img-fluid w-100" src="https://fabiopacifici.com/wp-content/uploads/2019/11/fabio_pacifici.jpg" alt="Fabio Pacifici Image">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <h1 class="py-5">Contact Me</h1>
            <ul class="list-unstyled">
                <li> <i class="fas fa-globe fa-lg fa-fw"></i> <a href="https://fabiopacifici.com" target="_blank" rel="noopener noreferrer">FabioPacifici.com</a></li>
                <li> <i class="fas fa-envelope fa-lg fa-fw"></i> <a href="https://fabiopacifici.com/contact-me" target="_blank" rel="noopener noreferrer">Email</a></li>
                <li> <i class="fab fa-wordpress fa-lg fa-fw"></i> <a href="http://profiles.wordpress.org/fabsere" target="_blank" rel="noopener noreferrer">WordPress.org</a></li>
                <li> <i class="fab fa-bitbucket fa-lg fa-fw"></i> <a href="http://bitbucket.org/fbhood" target="_blank" rel="noopener noreferrer">BitBuket</a></li>

            </ul>
        </div>
    </div>
</div>



<?php include("partials/footer.php"); ?>