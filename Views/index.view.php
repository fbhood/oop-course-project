<?php include("partials/head.php"); ?>

<!-- Nav Tabs -->
<ul class="nav nav-pills d-flex justify-content-center mb-5" id="myTab" role="tablist">
    <em class="d-flex align-items-center pr-5">Products: </em>
    <li class="nav-item">
        <a class="nav-link active" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="true">Products</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="bicycles-tab" data-toggle="tab" href="#bicycles" role="tab" aria-controls="bicycles" aria-selected="false">Bicycles</a>
    </li>

</ul>

<!-- Tab Panes -->
<div class="tab-content">
    <div class="tab-pane fade show active" id="products" role="tabpanel" aria-labelledby="products-tab">

        <div id="products" class="container">

            <div class="row">

                <?php


                if ($websites) {

                    foreach ($websites as $key => $item) {

                        include('partials/product-card.php');
                    }
                }


                ?>
            </div>

        </div>


    </div>
    <div class="tab-pane fade" id="bicycles" role="tabpanel" aria-labelledby="bicycles-tab">

        <div id="bicycles" class="container">

            <div class="row">

                <?php


                if ($bicycles) {

                    foreach ($bicycles as $key => $item) {

                        include('partials/product-card.php');
                    }
                }


                ?>
            </div>

        </div>


    </div>
</div>

<?php include("partials/footer.php"); ?>