<!doctype html>
<html lang="en">

<head>
    <title>PHP OOP Crash Course</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

    <style>
        html,
        body {
            font-size: 14px;
        }

        a {
            color: #343a40;
            font-weight: bold;
        }

        a:hover {
            color: #343a40;
            text-decoration: underline;
        }

        .nav-link {
            font-weight: lighter;
        }

        .nav-link:focus,
        .nav-link:hover {
            font-weight: bold;
            text-decoration: none;
        }

        .nav-pills .nav-link.active {
            background: #343a40;
        }

        .nav-pills .nav-link.active:hover {
            background: #263238;
        }

        .nav-active {
            font-weight: 900;
            text-decoration: underline;
        }

        .rounded-xl {
            border-radius: 1rem !important;
        }
    </style>


</head>

<body>

    <?php include("navbar.php"); ?>

    <header class="jumbotron jumbotron-fluid bg-dark text-white mt-5">
        <div class="container">
            <h1 class="text-center py-5 px-4">PHP OOP Crash course</h1>
        </div>
    </header>