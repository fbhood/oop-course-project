<nav class="nav justify-content-center fixed-top bg-white shadow py-4">
    <a class="nav-link <?php if ($_SERVER["REQUEST_URI"] == "/") {
                            echo "nav-active";
                        } else {
                            echo "";
                        } ?>" href="/">Home</a>
    <a class="nav-link <?php if ($_SERVER["REQUEST_URI"] == "/about") {
                            echo "nav-active";
                        } else {
                            echo "";
                        } ?>" href="/about">About</a>
    <a class="nav-link <?php if ($_SERVER["REQUEST_URI"] == "/contact") {
                            echo "nav-active";
                        } else {
                            echo "";
                        } ?>" href="/contact">Contact</a>
</nav>