<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">

    <article class="card shadow rounded-xl my-4">
        <header class="card-header bg-dark text-white text-center">
            <h3 class="card-title"> <?php echo $item->getName(); ?> </h3>

        </header>
        <a href="/<?php echo $item->uri; ?>">
            <img class="card-img-top" src="<?php echo $item->image; ?>" alt="<?php echo $item->getName(); ?>">
        </a>
        <p class="lead card-body">
            <?php echo $item->getDescription(); ?>

        </p>
        <p class="text-center"> <a class="btn btn-dark" href="/<?php echo $item->uri; ?>">View Product</a> </p>
        <footer class="card-footer rounded-xl bg-dark text-center text-white">
            <em> Price £ </em> <strong><?php echo $item->getPrice(); ?></strong>

        </footer>
    </article>

</div>