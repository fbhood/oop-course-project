<?php include("partials/head.php"); ?>

<div class="container">


    <div class="row">
        <div class="col-xs-12 col-6">
            <img class="w-100" src="<?php echo $image; ?>" alt="<?php echo $name; ?>">
        </div>
        <div class="col-xs-12 col-6">
            <h1><?php echo $name; ?></h1>

            <p class="lead"><?php echo $desciption; ?></p>


            <h4>$ <?php echo $price; ?></h4>




            <nav class="nav justify-content-center my-3">
                <a class="nav-link rounded bg-light shadow " href="<?php echo $prev; ?>">Prev</a>
                <a class="nav-link rounded bg-light shadow active" href="<?php echo $next; ?>">Next</a>

            </nav>

        </div>
    </div>






</div>

<?php include("partials/footer.php"); ?>