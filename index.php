<?php

require "./Classes/class.product.php";
require "./Classes/class.bicycle.php";
require "./Route.php";
require "./routes.php";
require "./products.php";


$route = new Route();

$routeList = $route->register($routes);

$uri = trim($_SERVER["REQUEST_URI"], "/");


/* 
Display the sigle products view page 
*/


//1. group all products objets in a single array
$products = [];
array_push($products, ...$websites);
array_push($products, ...$bicycles);

//2. create an array with all products uri
$productsUris = [];
for ($i = 0; $i < count($products); $i++) {

    $productsUris[] = $products[$i]->createUri();
    $products[$i]->setUri($productsUris[$i]);
}
//3. push products uri to the route list and assign to the singleController.php
for ($i = 0; $i < count($productsUris); $i++) {

    $routeList[$productsUris[$i]] = "./Controllers/singleController.php";
}


if (array_key_exists($uri, $routeList)) {
    require "$routeList[$uri]";
} else {
    require "./404.php";
    throw new Exception("Error Processing Request, the route does not exist", 404);
}
