<?php

$websites = [

    new Product("One page website", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200),
    new Product("Two page website", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200),
    new Product("Three page website", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200),
    new Product("Four page website", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200),
    new Product("Five page website", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200),
    new Product("Six page website", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200)

];
$websites[0]->setImage("https://source.unsplash.com/random/300x300/?coding");
$websites[1]->setImage("https://source.unsplash.com/random/300x300/?web");
$websites[2]->setImage("https://source.unsplash.com/random/300x300/?computer");
$websites[3]->setImage("https://source.unsplash.com/random/300x300/?agency");
$websites[4]->setImage("https://source.unsplash.com/random/300x300/?android");
$websites[5]->setImage("https://source.unsplash.com/random/300x300/?pc");


$bicycles = [

    new Bicycle("First Bicycle", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200, "red", 21.5, 28),
    new Bicycle("Second Bicycle", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200, "black", 21.5, 28),
    new Bicycle("Third Bicycle", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200, "white", 21.5, 28),
    new Bicycle("Fourth Bicycle", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200, "blue", 21.5, 28),
    new Bicycle("Fifth Bicycle", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200, "grey", 21.5, 28),
    new Bicycle("Sixth Bicycle", "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque dolorum quae fuga sint inventore tempore.", 1200, "green", 21.5, 28),


];
$bicycles[0]->setImage("https://source.unsplash.com/random/300x300/?bicycle-vehicle, black");
$bicycles[1]->setImage("https://source.unsplash.com/random/300x300/?bicycle-vehicle, red");
$bicycles[2]->setImage("https://source.unsplash.com/random/300x300/?bicycle-vehicle, white");
$bicycles[3]->setImage("https://source.unsplash.com/random/300x300/?bicycle-vehicle, blue");
$bicycles[4]->setImage("https://source.unsplash.com/random/300x300/?bicycle-vehicle, grey");
$bicycles[5]->setImage("https://source.unsplash.com/random/300x300/?bicycle-vehicle, green");
